package com.rajeev.playground.cerebro.ui.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.rajeev.playground.cerebro.R

class ChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val body:TextView
    var isQuestion:Boolean = false

    init {
        body = itemView.findViewById(R.id.text_message_body)
    }


}