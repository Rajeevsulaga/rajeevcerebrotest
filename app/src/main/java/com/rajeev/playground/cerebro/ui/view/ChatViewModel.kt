package com.rajeev.playground.cerebro.ui.view

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.LiveData



class ChatViewModel {

   var chat: MutableLiveData<MutableList<String>>? = null


    fun getChat(): LiveData<MutableList<String>>? {
        chat?.let { return it}

        chat = MutableLiveData()
        return chat
    }


}