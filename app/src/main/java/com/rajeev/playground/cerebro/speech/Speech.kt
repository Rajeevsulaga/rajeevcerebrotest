package com.rajeev.playground.cerebro.speech

import android.content.Context
import android.speech.tts.TextToSpeech
import android.speech.SpeechRecognizer
import java.util.*
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import kotlin.collections.ArrayList
import android.speech.RecognizerIntent
import android.os.Build
import android.content.Intent
import com.rajeev.playground.cerebro.speech.exception.GoogleVoiceTypingDisabledException
import com.rajeev.playground.cerebro.speech.exception.SpeechRecognitionNotAvailable
import kotlin.collections.HashMap


/**
 * @author rajeev.sangamnerkar
 */
class Speech private constructor() {

    private val LOG_TAG = Speech::class.java.simpleName

    init { println("This ($this) is a singleton") }

    companion object {

        @Volatile private var INSTANCE: Speech? = null
        fun initSpeech(context: Context)= INSTANCE ?: Speech(context).also { INSTANCE = it }
        fun getInstance(): Speech {
            if (INSTANCE == null)
                throw IllegalStateException("Speech recognition has not been initialized! call init method first!");

            return INSTANCE as Speech;
        }
    }


    private var mSpeechRecognizer: SpeechRecognizer? = null
    private var mCallingPackage: String? = null
    private var mPreferOffline = false
    private var mGetPartialResults = true
    private var mDelegate: SpeechDelegate? = null
    private var mIsListening = false

    private var mPartialData:List<String> = ArrayList()
    private var mUnstableData: String? = null

    private var mDelayedStopListening: DelayedOperation? = null
    private var mContext: Context? = null

    private var mTextToSpeech: TextToSpeech? = null
    private val mTtsCallbacks:MutableMap<String, TextToSpeechCallback> = HashMap()
    private var mLocale = Locale.getDefault()
    private var mTtsRate = 1.0f
    private var mTtsPitch = 1.0f
    private var mTtsQueueMode = TextToSpeech.QUEUE_FLUSH
    private var mStopListeningDelayInMs: Long = 10000
    private var mTransitionMinimumDelay: Long = 1200
    private var mLastActionTimestamp: Long = 0
    private var mLastPartialResults: List<String>? = null


    private val mTttsInitListener = TextToSpeech.OnInitListener { status ->
        when (status) {
            TextToSpeech.SUCCESS -> Log.i(LOG_TAG, "TextToSpeech engine successfully started")

            TextToSpeech.ERROR -> Log.e(LOG_TAG, "Error while initializing TextToSpeech engine!")

            else -> Log.e(LOG_TAG, "Unknown TextToSpeech status: " + status)
        }
    }

    private var mTtsProgressListener: UtteranceProgressListener? = null

    private val mListener = object : RecognitionListener {

        override fun onReadyForSpeech(bundle: Bundle) {
            mPartialData = ArrayList()
            mUnstableData = null
        }

        override fun onBeginningOfSpeech() {
//            if (mProgressView != null)
//                mProgressView.onBeginningOfSpeech()

            mDelayedStopListening!!.start(object : DelayedOperation.Operation {
                override fun onDelayedOperation() {
                    returnPartialResultsAndRecreateSpeechRecognizer()
                    Log.d("ReachedStop", "Stoppong")
                    //  mListenerDelay.onClick("1");
                }

                override fun shouldExecuteDelayedOperation(): Boolean {
                    return true
                }
            })
        }

        override fun onRmsChanged(v: Float) {
            try {
                mDelegate?.onSpeechRmsChanged(v)
            } catch (exc: Throwable) {
                Log.e(Speech::class.java.simpleName,
                        "Unhandled exception in delegate onSpeechRmsChanged", exc)
            }

//            if (mProgressView != null)
//                mProgressView.onRmsChanged(v)
        }

        override fun onPartialResults(bundle: Bundle) {
            mDelayedStopListening!!.resetTimer()

            val partialResults = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
            val unstableData = bundle.getStringArrayList("android.speech.extra.UNSTABLE_TEXT")

            if (partialResults != null && !partialResults.isEmpty()) {
                mPartialData = ArrayList()
                (mPartialData as ArrayList<String>).addAll(partialResults)
                mUnstableData = if (unstableData != null && !unstableData.isEmpty())
                    unstableData[0]
                else
                    null
                try {
                    if (mLastPartialResults == null || !mLastPartialResults!!.equals(partialResults)) {
                        mDelegate?.onSpeechPartialResults(partialResults)
                        mLastPartialResults = partialResults
                    }
                } catch (exc: Throwable) {
                    Log.e(Speech::class.java.simpleName,
                            "Unhandled exception in delegate onSpeechPartialResults", exc)
                }

            }
        }

        override fun onResults(bundle: Bundle) {
            mDelayedStopListening!!.cancel()

            val results = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)

            val result: String

            if (results != null && !results.isEmpty()
                    && results[0] != null && !results[0].isEmpty()) {
                result = results[0]
            } else {
                Log.i(Speech::class.java.simpleName, "No speech results, getting partial")
                result = getPartialResultsAsString()
            }

            mIsListening = false

            try {
                mDelegate?.onSpeechResult(result.trim { it <= ' ' })
            } catch (exc: Throwable) {
                Log.e(Speech::class.java.simpleName,
                        "Unhandled exception in delegate onSpeechResult", exc)
            }

//            if (mProgressView != null)
//                mProgressView.onResultOrOnError()

            initSpeechRecognizer(mContext)
        }

        override fun onError(code: Int) {
//            Log.e(LOG_TAG, "Speech recognition error", SpeechRecognitionException(code))
            returnPartialResultsAndRecreateSpeechRecognizer()
        }

        override fun onBufferReceived(bytes: ByteArray) {

        }

        override fun onEndOfSpeech() {
//            if (mProgressView != null)
//                mProgressView.onEndOfSpeech()
        }

        override fun onEvent(i: Int, bundle: Bundle) {

        }
    }

    private constructor(context: Context?) : this() {
        initSpeechRecognizer(context)
        initTts(context!!)
    }

    private constructor(context: Context?, callingPackage: String) : this() {
        initSpeechRecognizer(context)
        initTts(context!!)
        mCallingPackage = callingPackage
    }

    private fun initSpeechRecognizer(context: Context?) {
        if (context == null)
            throw IllegalArgumentException("context must be defined!")

        mContext = context

        if (SpeechRecognizer.isRecognitionAvailable(context)) {
                try {
                    mSpeechRecognizer!!.destroy()
                } catch (exc: Throwable) {
                    Log.d(Speech::class.java.simpleName,
                            "Non-Fatal error while destroying speech. " + exc.message)
                } finally {
                    mSpeechRecognizer = null
                }

            mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(context)
            mSpeechRecognizer!!.setRecognitionListener(mListener)
            initDelayedStopListening(context)

        } else {
            mSpeechRecognizer = null
        }

        mPartialData = ArrayList()
        mUnstableData = null
    }

    private fun initTts(context: Context) {
        if (mTextToSpeech == null) {
            mTtsProgressListener = mContext?.let { TtsProgressListener(it, mTtsCallbacks) }
            mTextToSpeech = TextToSpeech(context.applicationContext, mTttsInitListener)
            mTextToSpeech!!.setOnUtteranceProgressListener(mTtsProgressListener)
            mTextToSpeech!!.language = mLocale
            mTextToSpeech!!.setPitch(mTtsPitch)
            mTextToSpeech!!.setSpeechRate(mTtsRate)
        }
    }

    private fun initDelayedStopListening(context: Context) {
            mDelayedStopListening?.cancel()
            mDelayedStopListening = null
        //        Toast.makeText(context, "destroyed", Toast.LENGTH_SHORT).show();
            mListenerDelay?.onSpecifiedCommandPronounced("1")
        mDelayedStopListening = DelayedOperation(context, "delayStopListening", mStopListeningDelayInMs)
    }

    /**
     * Starts voice recognition.
     *
     * @param delegate delegate which will receive speech recognition events and status
     * @throws SpeechRecognitionNotAvailable      when speech recognition is not available on the device
     * @throws GoogleVoiceTypingDisabledException when google voice typing is disabled on the device
     */
    @Throws(SpeechRecognitionNotAvailable::class, GoogleVoiceTypingDisabledException::class)
    fun startListening(delegate: SpeechDelegate) {
        if (mIsListening) return

        if (mSpeechRecognizer == null)
            throw SpeechRecognitionNotAvailable()

        if (delegate == null)
            throw IllegalArgumentException("delegate must be defined!")

        if (throttleAction()) {
            Log.d(javaClass.simpleName, "Hey man calm down! Throttling start to prevent disaster!")
            return
        }
        //
        //        if (progressView != null && !(progressView.getParent() instanceof LinearLayout))
        //            throw new IllegalArgumentException("progressView must be put inside a LinearLayout!");
        //
        //        mProgressView = progressView;
        mDelegate = delegate

        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
                .putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1)
                .putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, mGetPartialResults)
                .putExtra(RecognizerIntent.EXTRA_LANGUAGE, mLocale.language)
                .putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)

        if (mCallingPackage != null && !mCallingPackage!!.isEmpty()) {
            intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, mCallingPackage)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, mPreferOffline)
        }

        try {
            mSpeechRecognizer!!.startListening(intent)
        } catch (exc: SecurityException) {
            throw GoogleVoiceTypingDisabledException()
        }

        mIsListening = true
        updateLastActionTimestamp()

        try {
            mDelegate?.onStartOfSpeech()
        } catch (exc: Throwable) {
            Log.e(Speech::class.java.simpleName,
                    "Unhandled exception in delegate onStartOfSpeech", exc)
        }

    }

    private fun unregisterDelegate() {
        mDelegate = null
//        mProgressView = null
    }

    private fun updateLastActionTimestamp() {
        mLastActionTimestamp = Date().time
    }

    private fun throttleAction(): Boolean {
        return Date().time <= mLastActionTimestamp + mTransitionMinimumDelay
    }

    /**
     * Stops voice recognition listening.
     * This method does nothing if voice listening is not active
     */
    fun stopListening() {
        if (!mIsListening) return

        if (throttleAction()) {
            Log.d(javaClass.simpleName, "Hey man calm down! Throttling stop to prevent disaster!")
            return
        }

        mIsListening = false
        updateLastActionTimestamp()
        returnPartialResultsAndRecreateSpeechRecognizer()
    }

    private fun getPartialResultsAsString(): String {
        val out = StringBuilder("")

        for (partial in mPartialData) {
            out.append(partial).append(" ")
        }

        if (mUnstableData != null && !mUnstableData!!.isEmpty())
            out.append(mUnstableData)

        return out.toString().trim { it <= ' ' }
    }

    private fun returnPartialResultsAndRecreateSpeechRecognizer() {
        mIsListening = false
        try {
            mDelegate?.onSpeechResult(getPartialResultsAsString())
        } catch (exc: Throwable) {
            Log.e(Speech::class.java.simpleName,
                    "Unhandled exception in delegate onSpeechResult", exc)
        }

        //        if (mProgressView != null)
        //            mProgressView.onResultOrOnError();

        // recreate the speech recognizer
        initSpeechRecognizer(mContext)
    }

    /**
     * Check if voice recognition is currently active.
     *
     * @return true if the voice recognition is on, false otherwise
     */
    fun isListening(): Boolean {
        return mIsListening
    }

    /**
     * Uses text to speech to transform a written message into a sound.
     *
     * @param message message to play
     */
    fun say(message: String) {
        say(message, null)
    }

    /**
     * Uses text to speech to transform a written message into a sound.
     *
     * @param message  message to play
     * @param callback callback which will receive progress status of the operation
     */
    fun say(message: String, callback: TextToSpeechCallback?) {

        val utteranceId = UUID.randomUUID().toString()

        if (callback != null) {
            mTtsCallbacks.put(utteranceId, callback)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTextToSpeech!!.speak(message, mTtsQueueMode, null, utteranceId)
        } else {
            val params = Bundle()
            params.putCharSequence(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mTextToSpeech!!.speak(message,mTtsQueueMode, null, utteranceId)
            }
        }
    }

    /**
     * Stops text to speech.
     */
    fun stopTextToSpeech() {
        mTextToSpeech!!.stop()
    }

    /**
     * Set whether to only use an offline speech recognition engine.
     * The default is false, meaning that either network or offline recognition engines may be used.
     *
     * @param preferOffline true to prefer offline engine, false to use either one of the two
     * @return speech instance
     */
    fun setPreferOffline(preferOffline: Boolean): Speech {
        mPreferOffline = preferOffline
        return this
    }

    /**
     * Set whether partial results should be returned by the recognizer as the user speaks
     * (default is true). The server may ignore a request for partial results in some or all cases.
     *
     * @param getPartialResults true to get also partial recognition results, false otherwise
     * @return speech instance
     */
    fun setGetPartialResults(getPartialResults: Boolean): Speech {
        mGetPartialResults = getPartialResults
        return this
    }

    /**
     * Sets text to speech and recognition language.
     * Defaults to device language setting.
     *
     * @param locale new locale
     * @return speech instance
     */
    fun setLocale(locale: Locale): Speech {
        mLocale = locale
        mTextToSpeech!!.setLanguage(locale)
        return this
    }

    /**
     * Sets the speech rate. This has no effect on any pre-recorded speech.
     *
     * @param rate Speech rate. 1.0 is the normal speech rate, lower values slow down the speech
     * (0.5 is half the normal speech rate), greater values accelerate it
     * (2.0 is twice the normal speech rate).
     * @return speech instance
     */
    fun setTextToSpeechRate(rate: Float): Speech {
        mTtsRate = rate
        mTextToSpeech!!.setSpeechRate(rate)
        return this
    }

    /**
     * Sets the speech pitch for the TextToSpeech engine.
     * This has no effect on any pre-recorded speech.
     *
     * @param pitch Speech pitch. 1.0 is the normal pitch, lower values lower the tone of the
     * synthesized voice, greater values increase it.
     * @return speech instance
     */
    fun setTextToSpeechPitch(pitch: Float): Speech {
        mTtsPitch = pitch
        mTextToSpeech!!.setPitch(pitch)
        return this
    }

    /**
     * Sets the idle timeout after which the listening will be automatically stopped.
     *
     * @param milliseconds timeout in milliseconds
     * @return speech instance
     */
    fun setStopListeningAfterInactivity(milliseconds: Long): Speech {
        mStopListeningDelayInMs = milliseconds
        initDelayedStopListening(mContext!!)
        return this
    }

    /**
     * Sets the minimum interval between start/stop events. This is useful to prevent
     * monkey input from users.
     *
     * @param milliseconds minimum interval betweeb state change in milliseconds
     * @return speech instance
     */
    fun setTransitionMinimumDelay(milliseconds: Long): Speech {
        mTransitionMinimumDelay = milliseconds
        return this
    }

    /**
     * Sets the text to speech queue mode.
     * By default is TextToSpeech.QUEUE_FLUSH, which is faster, because it clears all the
     * messages before speaking the new one. TextToSpeech.QUEUE_ADD adds the last message
     * to speak in the queue, without clearing the messages that have been added.
     *
     * @param mode It can be either TextToSpeech.QUEUE_ADD or TextToSpeech.QUEUE_FLUSH.
     * @return speech instance
     */
    fun setTextToSpeechQueueMode(mode: Int): Speech {
        mTtsQueueMode = mode
        return this
    }

    private var mListenerDelay: Speech.stopDueToDelay? = null

    // define listener
    interface stopDueToDelay {
        fun onSpecifiedCommandPronounced(event: String)
    }

    // set the listener. Must be called from the fragment
    fun setListener(listener: Speech.stopDueToDelay) {
        this.mListenerDelay = listener
    }


}
