package com.rajeev.playground.cerebro

/**
 * @author rajeev.sangamnerkar
 */
public class SampleSingleton private constructor() {
    init { println("This ($this) is a singleton") }

    private object Holder { val INSTANCE = SampleSingleton() }

    companion object {
        val instance: SampleSingleton by lazy { Holder.INSTANCE }
    }
    var b = 0
}