package com.rajeev.playground.cerebro.speech

/**
 * @author rajeev.sangamnerkar
 */
interface TextToSpeechCallback {
    fun onStart()
    fun onCompleted()
    fun onError()
}