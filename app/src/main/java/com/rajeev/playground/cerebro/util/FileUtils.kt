package com.rajeev.playground.cerebro.util

import android.content.Context
import com.rajeev.playground.cerebro.speech.record.RecordManager


 fun getFilePath(context :Context?):String{
    return context?.getExternalFilesDir(null)?.path +"/"+ RecordManager.CONV_LOG
}
